﻿namespace TestArraysClass
{
    public class Bai14
    {
        //14 thực hiện hiển thị categoryName
        public static List<Product> mapProductByCategoryName(List<Product> listProduct, List<Category> listCategory)
        {
            for (int i = 0; i < listProduct.Count; i++)
            {
                for (int j = 0; j < listCategory.Count; j++)
                {
                    if (listProduct[i].cateogryId == listCategory[j].id)
                    {
                        listProduct[i].categoryName = listCategory[j].name;
                    }
                }
            }
            return listProduct;
        }
    }
}
