﻿using System;
using System.Text;
namespace TestArraysClass
{
	public class Product
	{
		public string name;
		public int price;
		public int quantity;
		public int cateogryId;
		public string categoryName;

		public Product()
		{
		}
		public Product(string name, int price, int quantity, int categoryId)
		{
			this.name = name;
			this.price = price;
			this.quantity = quantity;
			this.cateogryId = categoryId;
		}
        public void show()
        {
            Console.WriteLine("ten san pham : " + name);
            Console.WriteLine("gia san pham : " + price); 
            Console.WriteLine("so luong san pham : " + quantity);
            Console.WriteLine("ma loai san pham : " + cateogryId);
            Console.WriteLine();
        }
		public void showCategoryName()
		{
			Console.WriteLine("Tên sản phẩm : " + name);
			Console.WriteLine("gia san pham : " + price);
			Console.WriteLine("so luong san pham : " + quantity);
			Console.WriteLine("ma loai san pham : " + cateogryId);
			Console.WriteLine("Ten ma loai san pham : " + categoryName);
			Console.WriteLine();
		}
	}
	public class Menu
    {
		public int id;
		public string title;
        public int parentId;
		public Menu(int id ,string title,int parentId)
        {
			this.id = id;
			this.title = title;
			this.parentId = parentId;
        }
		public void show()
        {
			Console.WriteLine(title);
        }
    }

    public class Category
    {
		public int id;
		public String name;
		public Category(int id ,String name)
        {
			this.id = id;
			this.name = name;
        }
		public void showCategory()
        {
			Console.WriteLine("ma loai san pham : " + id);
			Console.WriteLine("Ten loai san pham :" + name);
        }
    }

    public	class Program
	{
		public static string getbyId(List<Category> category, int categoryId)
		{
			if (categoryId > 0)
			{
				for (int i = 0; i < category.Count; i++)
				{
					if (categoryId == category[i].id)
					{
						return category[i].name;
					}
				}
			}	
				return "id id khong nen dat null hay am";		
		}
		static void Main()
		{
			Product product = new Product();
			//thực hiện gán dữ liệu
			List<Product> pro = new List<Product>(){
			new Product("CPU", 750, 10, 1),
			new Product("RAM", 50, 2, 2),
			new Product("HDD", 70, 1, 2),
			new Product("Main", 400, 3, 1),
			new Product("KEYBOARD", 8, 10, 4),
			new Product("MOUSE", 25, 50, 4),
			new Product("VGA", 60, 35, 3),
			new Product("MONITOR", 120, 28, 2),
			new Product("CASE", 120, 28, 5)
		};
			List<Menu> Menus = new List<Menu>()
			{
			new Menu(1,"The thao",0),
			new Menu(2, "Xa Hoi", 0),
			new Menu(3,"The thao trong nuoc",1),
			new Menu(4,"Giao thong",2),
			new Menu(5,"Moi truong",2),
			new Menu(6,"the thao quoc te",1),
			new Menu(7,"Moi truong do thi",5),
			new Menu(8,"giap thong un tac",4)
            };

			List<Category> categories = new List<Category>()
			{
			new Category(1,"Comuter"),
			new Category(2, "Memmory"),
			new Category(3,"Card"),
			new Category(4,"Acsesory")
			};

			/*void swap(ref Product a1, ref Product b1)
			{
				Product temp = a1;
				a1 = b1;
				b1 = temp;
			}*/

			Console.WriteLine("tim kiem theo ten");
			List<Product> res = new List<Product>();
			product = Bai4.findByName(pro, "RAM");
			product.show();
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("tim kiem theo loai san pham");
			List<Product> pr = new List<Product>();
			pr = Bai5.findProductByCategory(1, pro);
			for (int i = 0; i < pr.Count; i++)
			{
				pr[i].show();
			};
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("tim kiem theo gia nho hon");
			pr = Bai6.findProductByPrice(200, pro);
			for (int i = 0; i < pr.Count; i++)
			{
				pr[i].show();
			}
			//sắp xếp bubble sort
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("sap xep  bubblesort");

			pr = Bai11.sortByPrice(pro);

			for (int i = 0; i < pro.Count; i++)
			{
				pr[i].show();
			}
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("sap xep insertsort");
			pr = Bai12.sortByName(pro);
			for (int i = 0; i < pro.Count; i++)
			{
				pr[i].show();
			}
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("sap xep insertSort theo ten loai san pham");
			pr = Bai13.sortByCategoryName(pro, categories);
			for (int i = 0; i < pro.Count; i++)
			{
				pr[i].show();
			}
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("sap xep hiên thi categoryName");
			pr = Bai14.mapProductByCategoryName(pro, categories);
			for (int i = 0; i < pro.Count; i++)
			{
				pro[i].showCategoryName();
			}
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("lấy ra san pham co gia tri nho nhat");
			product = Bai15.minByProduct(pro);
			product.show();
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("lay ra san pham co gia tri lon nhat");
			product = Bai16.maxByProduct(pro);
			product.show();
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("Tính lương theo năm");
			float b = bai21.calSalary(10, 2);
			Console.WriteLine(b);
			float c = bai21.nocalSalary(10, 2);
			Console.WriteLine(c);
			Console.WriteLine("--------------------------------------------------------");
			Console.WriteLine("Tinh so lai theo theo thang");
			double e = Bai22.calMoth(10, 20);
			Console.WriteLine(e);
			double h = Bai22.calMoth(10, 20);
			Console.WriteLine(h);
			List<Menu> list = new List<Menu>();
			list = Bai23.printMenu(Menus);
		    for(int i = 0; i < list.Count; i++)
            {
				list[i].show();	
            }
		}
		
	}
}