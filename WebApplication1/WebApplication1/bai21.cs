﻿namespace TestArraysClass
{
    public class bai21
    {
        //hàm tình lương sử dụng đệ quy
      public static float calSalary(float salary,int n)
        {
            float salaryCurrent = 0;
                if (n == 0)
                {
                    return salary;
                }
            salaryCurrent  = calSalary(salary, n - 1)  +calSalary(salary,n-1) *0.1f;
            return salaryCurrent;
        }
        //thực hiện tính lương không đệ quy
      public static float nocalSalary(float salary,int n)
        {
            float salaryCurrent =0;
            for (int i = 0; i < n; i++)
            {
                if(i == n)
                {
                    salaryCurrent = salary;
                }
                else
                {
                    salaryCurrent = salary + salary * 0.1f;
                }
            }
            return salaryCurrent; 
        }
    }
}
