﻿namespace TestArraysClass
{
    public class Bai13
    {
		//13 thực hiện sắp xếp sản phẩm theo tên mã loại sản phẩm
		public static List<Product> sortByCategoryName(List<Product> listProduct, List<Category> listCategory)
		{
			for (int i = 1; i < listProduct.Count; ++i)
			{
				int keyId = listProduct[i].cateogryId;
				var nameCate =Program.getbyId(listCategory, keyId);
				var productRoot = listProduct[i];
				int j = i - 1;
				while (j >= 0 &&Program.getbyId(listCategory, listProduct[j].cateogryId).CompareTo(nameCate) >= 0)
				{
					listProduct[j + 1] = listProduct[j];
					j = j - 1;
				}
				listProduct[j + 1] = productRoot;
			}
			return listProduct;
		}
	}
}
