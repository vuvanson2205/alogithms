﻿namespace TestArraysClass
{
    public class Bai12
    {
		//thưc hiện sắp xếp insert sort
		public static List<Product> sortByName(List<Product> listProduct)
		{
			for (int i = 1; i < listProduct.Count; ++i)
			{
				Product product = listProduct[i];
				int j = i - 1;
				while (j >= 0 && listProduct[j].name.Length > product.name.Length)
				{
					listProduct[j + 1].name = listProduct[j].name;
					j = j - 1;
				}
				listProduct[j + 1] = product;
			}
			return listProduct;
		}
	}
}
