﻿namespace TestArraysClass
{
    public class Bai15
    {
        //15 thực hiện hiển thị product có giá nhỏ nhất
        public static Product minByProduct(List<Product> lisproduct)
        {
            int minPrice = lisproduct[0].price;
            Product product = new Product();
            product = lisproduct[0];
            for (int i = 0; i < lisproduct.Count; i++)
            {
                if (lisproduct[i].price < minPrice)
                {
                    product = lisproduct[i];
                }
            }
            return product;
        }
    }
}
