﻿namespace TestArraysClass
{
    public class Bai11
    {
		//thực hiện sắp xếp bubblesort
		public static List<Product> sortByPrice(List<Product> listProduct)
		{
			int i, j;
			for (i = 0; i < listProduct.Count - 1; i++)
			{
				for (j = i + 1; j < listProduct.Count; j++)
				{
					if (listProduct[i].price > listProduct[j].price)
					{
						Product pro1 = listProduct[i];
						listProduct[i] = listProduct[j];
						listProduct[j] = pro1;
					}
				}
			}
			return listProduct;
		}
	}
}
