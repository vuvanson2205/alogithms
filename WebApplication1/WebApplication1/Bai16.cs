﻿namespace TestArraysClass
{
    public class Bai16
    {
		// 16 thực hiện lấy ra sản phẩm giá cao nhất
		public static Product maxByProduct(List<Product> lisproduct)
		{
			int maxPrice = lisproduct[0].price;
			Product product = new Product();
			product = lisproduct[0];
			for (int i = 0; i < lisproduct.Count; i++)
			{
				if (lisproduct[i].price > maxPrice)
				{
					product = lisproduct[i];
				}
			}
			return product;
		}
	}
}
