﻿namespace TestArraysClass
{
    public class Bai6
    {
		//thực hiện tìm kiếm theo giá
		public static List<Product> findProductByPrice(int price, List<Product> listProduct)
		{

			List<Product> products = new List<Product>();
			foreach (Product res in listProduct)
			{
				if (res.price <= price)
				{
					products.Add(res);
				}
			}
			return products;
		}
	}
}
